import java.util.ArrayList;
import java.util.List;


public class DueDateTester {

	/**
	 * Tester KLasse fuer DueDate. Es werden 6 verschiedene Daten ueberprueft. 
	 * Je nach Zykluslaenge kann der Geburtstermin variieren. 
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		DueDate menstrualDate1 = new DueDate(21,11,10);
		DueDate menstrualDate2 = new DueDate(7,4,11);
		DueDate menstrualDate3 = new DueDate(9,2,11);
		DueDate menstrualDate4 = new DueDate(9,1,11);
		DueDate menstrualDate5 = new DueDate(8,5,9);
		DueDate menstrualDate6 = new DueDate(17,9,12);
		
		List<DueDate> dateList = new ArrayList<DueDate>();
		
		dateList.add(menstrualDate1);
		dateList.add(menstrualDate2);
		dateList.add(menstrualDate3);
		dateList.add(menstrualDate4);
		dateList.add(menstrualDate5);
		dateList.add(menstrualDate6);
		
		for (DueDate dueDate : dateList) {
			System.out.println("\nMenstrual date: \t\t" + dueDate.GetLastPeriodDate());
			System.out.print("Due date standard: " + dueDate.getDueDate());
			System.out.println("\tDue date Naegele: " + dueDate.getDueDateNaegele());
		}
		

		
	}

}
