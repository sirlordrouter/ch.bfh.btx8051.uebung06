import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Berner Fachhochschule</br>
 * Medizininformatik BSc</br>
 * Modul 8051-HS2012</br>
 * 
 *<p>Provides calculations for a due date with a given date 
 *of the first day of the last period. The last Day of the Period can be 
 *returned as formatted Timestring. </p>
 *
 * @author Johannes Gnaegi, gnaegj1@bfh.ch
 * @version 09-10.2012
 */
public class DueDate {
	
	private GregorianCalendar lastPeriod; 

	/**
	 * Creates a new DueDate-Object with the Date of the 
	 * first Day of the last Period. 
	 * 
	 * @param aDay - day in month
	 * @param aMonth - month of year
	 * @param aYear - year
	 */
	public DueDate(int aDay, int aMonth, int aYear)
	{
		//aMonth - 1, da 0 = Januar
		lastPeriod = new GregorianCalendar(aYear, aMonth-1, aDay);
	}
	
	/**
	 * Returns the first day of last Period set. 
	 * 
	 * @return formatted Datestring with the Date of the last Period. 
	 */
	public String GetLastPeriodDate()
	{
		return FormatDate(lastPeriod.getTime()); 
	}
	
	/**
	 * Calculates the due date with the given date 
	 * of the first day of the last period. 
	 * 
	 * @return String - the formatted Date string of the calculated
	 * due date. 
	 */
	public String getDueDate()
	{
     	GregorianCalendar lastPeriodClone = new GregorianCalendar();
		lastPeriodClone = (GregorianCalendar) lastPeriod.clone();
		lastPeriodClone.add(Calendar.WEEK_OF_YEAR, 40); 		
		return FormatDate(lastPeriodClone.getTime());
	}

	/**
	 * Calculates the due date with the given date 
	 * of the first day of the last period. The method used
	 * is those from Naegele. 
	 * 
	 * @return String - the formatted Date string of the calculated
	 * due date. 
	 */
	public String getDueDateNaegele()
	{
		GregorianCalendar lastPeriodClone = new GregorianCalendar();
		lastPeriodClone = (GregorianCalendar) lastPeriod.clone();
		lastPeriodClone.add(Calendar.DATE, 7);
		lastPeriodClone.add(Calendar.MONTH, -3);
		lastPeriodClone.add(Calendar.YEAR, 1);
		return FormatDate(lastPeriodClone.getTime());	
	}
	
	/*
	 * Returns a given Date in the specified String pattern.  
	 */
	private String FormatDate(Date date) {

		SimpleDateFormat format = new SimpleDateFormat("E, dd.MM.yy"); 
		String dateString = format.format(date); 
		return dateString;
	}
}
